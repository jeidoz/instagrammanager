﻿using InstagramApiSharp.Classes;
using InstagramToolKit;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;

namespace InstagramManager
{
    /// <summary>
    /// Interaction logic for AuthWnd.xaml
    /// </summary>
    public partial class AuthWnd : Window
    {
        public AuthWnd()
        {
            LoadRememberedSession();
            InitializeComponent();
            Instagram.InitializeLogging(this);
        }

        private async void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrWhiteSpace(this.txtUsername.Text) ||
              string.IsNullOrWhiteSpace(this.txtPassword.Text))
            {
                MessageBox.Show("Fill required fields for login!", "Empty login fields", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            UserSessionData userSession = new UserSessionData
            {
                UserName = this.txtUsername.Text,
                Password = this.txtPassword.Text
            };

            Instagram.InitializeApi(userSession);
            Instagram.LoadSession();

            if(!Instagram.Api.IsUserAuthenticated)
            {
                var logInResult = await Instagram.Api.LoginAsync();
                Debug.WriteLine(logInResult.Value);
                if (logInResult.Succeeded)
                {
                    OpenMainWindow();
                    Instagram.SaveSession();
                }
                else
                {
                    if (logInResult.Value == InstaLoginResult.ChallengeRequired)
                    {
                        var challenge = await Instagram.Api.GetChallengeRequireVerifyMethodAsync();
                        if (challenge.Succeeded)
                        {
                            if (challenge.Value.SubmitPhoneRequired)
                            {
                                this.gridPhoneVerify.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                if (challenge.Value.StepData != null)
                                {
                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.PhoneNumber))
                                    {
                                        this.radioPhoneChallenge.Visibility = Visibility.Visible;
                                        this.radioPhoneChallenge.Content += $" ({challenge.Value.StepData.PhoneNumber})";
                                    }
                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.Email))
                                    {
                                        this.radioEmailChallenge.Visibility = Visibility.Visible;
                                        this.radioEmailChallenge.Content += $" ({challenge.Value.StepData.Email})";
                                    }

                                    this.gridChallenges.Visibility = Visibility.Visible;
                                }
                            }
                        }
                        else
                            MessageBox.Show(challenge.Info.Message, "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else if (logInResult.Value == InstaLoginResult.TwoFactorRequired)
                    {
                        this.gridTwoFactor.Visibility = Visibility.Visible;
                    }
                }
            }
            else
            {
                OpenMainWindow();
            }
        }
        private async void BtnSendPhoneCode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.txtPhoneNumber.Text))
                {
                    MessageBox.Show("Please type a valid phone number(with country code).\r\ni.e: +380951234568", "ERR", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    return;
                }
                var phoneNumber = this.txtPhoneNumber.Text;
                if (!phoneNumber.StartsWith("+"))
                    phoneNumber = $"+{phoneNumber}";

                var submitPhone = await Instagram.Api.SubmitPhoneNumberForChallengeRequireAsync(phoneNumber);
                if (submitPhone.Succeeded)
                {
                    this.gridVerify.Visibility = Visibility.Visible;
                    this.gridPhoneVerify.Visibility = Visibility.Collapsed;
                }
                else
                    MessageBox.Show(submitPhone.Info.Message, "ERR", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "EX", MessageBoxButton.OK, MessageBoxImage.Error); }
        }
        private async void BtnSelectChallenge_Click(object sender, RoutedEventArgs e)
        {
            bool isEmail = false;
            if (this.radioEmailChallenge.IsChecked.Value)
                isEmail = true;

            try
            {
                // Note: every request to this endpoint is limited to 60 seconds                 
                if (isEmail)
                {
                    // send verification code to email
                    var email = await Instagram.Api.RequestVerifyCodeToEmailForChallengeRequireAsync();
                    if (email.Succeeded)
                    {
                        MessageBox.Show($"We sent verify code to this email:\n{email.Value.StepData.ContactPoint}", "Verify code sent", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.gridVerify.Visibility = Visibility.Visible;
                        this.gridChallenges.Visibility = Visibility.Collapsed;
                    }
                    else
                        MessageBox.Show(email.Info.Message, "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    // send verification code to phone number
                    var phoneNumber = await Instagram.Api.RequestVerifyCodeToSMSForChallengeRequireAsync();
                    if (phoneNumber.Succeeded)
                    {
                        MessageBox.Show($"We sent verify code to this phone number(it's end with this):\n{phoneNumber.Value.StepData.ContactPoint}", "Verify code sent", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.gridVerify.Visibility = Visibility.Visible;
                        this.gridChallenges.Visibility = Visibility.Collapsed;
                    }
                    else
                        MessageBox.Show(phoneNumber.Info.Message, "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "EX", MessageBoxButton.OK, MessageBoxImage.Error); }
        }
        private async void BtnResendCode_Click(object sender, RoutedEventArgs e)
        {
            bool isEmail = false;
            if (this.radioEmailChallenge.IsChecked.Value)
                isEmail = true;

            try
            {
                // Note: every request to this endpoint is limited to 60 seconds                 
                if (isEmail)
                {
                    // send verification code to email
                    var email = await Instagram.Api.RequestVerifyCodeToEmailForChallengeRequireAsync(replayChallenge: true);
                    if (email.Succeeded)
                    {
                        MessageBox.Show($"We sent verification code one more time\r\nto this email:\n{email.Value.StepData.ContactPoint}", "Verify code resent", MessageBoxButton.OK, MessageBoxImage.Information); ;
                        this.gridVerify.Visibility = Visibility.Visible;
                        this.gridChallenges.Visibility = Visibility.Collapsed;
                    }
                    else
                        MessageBox.Show(email.Info.Message, "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    // send verification code to phone number
                    var phoneNumber = await Instagram.Api.RequestVerifyCodeToSMSForChallengeRequireAsync(replayChallenge: true);
                    if (phoneNumber.Succeeded)
                    {
                        MessageBox.Show($"We sent verification code one more time\r\nto this phone number(it's end with this):{phoneNumber.Value.StepData.ContactPoint}", "Verify code resent", MessageBoxButton.OK, MessageBoxImage.Information); ;
                        this.gridVerify.Visibility = Visibility.Visible;
                        this.gridChallenges.Visibility = Visibility.Collapsed;
                    }
                    else
                        MessageBox.Show(phoneNumber.Info.Message, "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "EX", MessageBoxButton.OK, MessageBoxImage.Error); }
        }
        private async void BtnVerify_Click(object sender, RoutedEventArgs e)
        {
            this.txtVerifyCode.Text = this.txtVerifyCode.Text.Trim();
            this.txtVerifyCode.Text = this.txtVerifyCode.Text.Replace(" ", "");
            var regex = new Regex(@"^-*[0-9,\.]+$");
            if (!regex.IsMatch(txtVerifyCode.Text))
            {
                MessageBox.Show("Verification code is numeric!!!", "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtVerifyCode.Text.Length != 6)
            {
                MessageBox.Show("Verification code must be 6 digits!!!", "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            try
            {
                // Note: calling VerifyCodeForChallengeRequireAsync function, 
                // if user has two factor enabled, will wait 15 seconds and it will try to
                // call LoginAsync.

                var verifyLogin = await Instagram.Api.VerifyCodeForChallengeRequireAsync(txtVerifyCode.Text);
                this.gridVerify.Visibility = Visibility.Collapsed;
                this.gridChallenges.Visibility = Visibility.Collapsed;
                if (verifyLogin.Succeeded)
                {
                    // you are logged in sucessfully.
                    // Save session
                    Instagram.SaveSession();
                    OpenMainWindow();
                }
                else
                {
                    // two factor is required
                    if (verifyLogin.Value == InstaLoginResult.TwoFactorRequired)
                    {
                        this.gridTwoFactor.Visibility = Visibility.Visible;
                    }
                    else
                        MessageBox.Show(verifyLogin.Info.Message, "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "EX", MessageBoxButton.OK, MessageBoxImage.Error); }
        }
        private async void BtnSendTwoFactorCode_Click(object sender, RoutedEventArgs e)
        {
            if (Instagram.Api == null)
                return;
            if (string.IsNullOrEmpty(this.txtTwoFactorCode.Text))
            {
                MessageBox.Show("Please type your two factor code and then press Auth button.", "", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            // send two factor code
            var twoFactorLogin = await Instagram.Api.TwoFactorLoginAsync(this.txtTwoFactorCode.Text);
            Debug.WriteLine(twoFactorLogin.Value);
            if (twoFactorLogin.Succeeded)
            {
                // connected
                // save session
                Instagram.SaveSession();
                this.gridTwoFactor.Visibility = Visibility.Collapsed;
                OpenMainWindow();
            }
            else
            {
                MessageBox.Show(twoFactorLogin.Info.Message, "ERR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private async void OpenMainWindow()
        {
            var currentUser = await Instagram.Api.GetCurrentUserAsync();
            MessageBox.Show($"You have been successfully authorized as @{currentUser.Value.UserName}!", "Successfull login", MessageBoxButton.OK, MessageBoxImage.Information);
            var mainWnd = new MainWindow();
            mainWnd.Show();
            this.Close();
        }
        private void LoadRememberedSession()
        {
            if (Instagram.IsSessionFileExists())
            {
                Instagram.InitializeApi();
                Instagram.LoadSession();
                if (Instagram.Api.IsUserAuthenticated)
                    OpenMainWindow();
                else
                    File.Delete(Instagram.GetSessionFilePath());
            }
        }

        private void BtnLoadSession_Click(object sender, RoutedEventArgs e)
        {
            var openFileDlg = new OpenFileDialog
            {
                Filter = "Session file|*.bin",
                Title = "Select session file",
                Multiselect = false,
                DefaultExt = "bin"
            };

            if(openFileDlg.ShowDialog() == true)
            {
                File.Copy(openFileDlg.FileName, Instagram.GetSessionFilePath(), true);
                Instagram.InitializeApi();
                Instagram.LoadSession();
                if (Instagram.Api.IsUserAuthenticated)
                    OpenMainWindow();
                else
                {
                    File.Delete(Instagram.GetSessionFilePath());
                    MessageBox.Show("Can't load session. May be it is outdated or corrupted?", "Can't load session", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }
}

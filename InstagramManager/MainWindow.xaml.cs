﻿using InstagramToolKit;
using InstagramToolKit.Events;
using InstagramToolKit.Exceptions;
using Microsoft.Win32;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace InstagramManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ToolKit _toolKit = new ToolKit();

        public MainWindow()
        {
            InitializeToolkitEventHandlers();
            InitializeComponent();
            this.Closing += MainWindow_Closing;
            Instagram.InitializeLogging(this);
        }
        private void InitializeToolkitEventHandlers()
        {
            _toolKit.Spammer.UserGotMessageEvent += SpammerPrintLog;
            _toolKit.Spammer.ChangedLeftUsersAmountEvent += SpammerChangedLeftUsersAmount;
            _toolKit.Spammer.SavingLeftUsersEvent += SpammerPrintLog;
            _toolKit.Spammer.PauseStartedEvent += SpammerPrintLog;
            _toolKit.Spammer.SendingStopedEvent += SpammerPrintLog;

            _toolKit.ConvertingLogEvent += ConverterUsernamesPrintLog;
            _toolKit.ConvertingProgressBarInitEvent += ConverterUsernamesProgressBarInit;
            _toolKit.ConvertingUserProcessedEvent += ConverterUsernamesUserProcesses;
        }

        #region UI Updaters
        private void PrintSpammerLog(string message)
        {
            UpdateUi(new Action(() =>
            {
                this.txtSpammerLogConsole.AppendText($"{message}{Environment.NewLine}");
                this.txtSpammerLogConsole.ScrollToEnd();
            }));
        }
        private void PrintConverterUsernamesLog(string message)
        {
            UpdateUi(new Action(() =>
            {
                this.txtConverterUsernamesConsole.AppendText($"{message}{Environment.NewLine}");
                this.txtConverterUsernamesConsole.ScrollToEnd();
            }));
        }
        private void UpdateUi(Action action)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, action);
        }
        #endregion

        #region Event Handlers
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.menuSessionRemember.IsChecked)
            {
                if (Instagram.IsSessionFileExists())
                {
                    File.Delete(Instagram.GetSessionFilePath());
                }
            }
        }
        private void SpammerPrintLog(object sender, AsyncProcessResult<string> args)
        {
            PrintSpammerLog(args.Message);
        }
        private void SpammerChangedLeftUsersAmount(object sender, AsyncProcessResult<int> args)
        {
            UpdateUi(new Action(() => this.lbSpammerUsersLeftCounter.Text = $"Users left: {args.Message}"));
        }

        private void ConverterUsernamesPrintLog(object sender, AsyncProcessResult<string> args)
        {
            PrintConverterUsernamesLog(args.Message);
        }
        private void ConverterUsernamesProgressBarInit(object sender, AsyncProcessResult<int> args)
        {
            UpdateUi(new Action(() => this.progressConverterUsernames.Maximum = args.Message));
        }
        private void ConverterUsernamesUserProcesses(object sender, EventArgs args)
        {
            UpdateUi(new Action(() => this.progressConverterUsernames.Value++));
        }
        #endregion

        #region Input Validators
        private void NumSpammerUsersPerStack_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }
        #endregion

        #region Menu
        private async void MenuSessionLogout_Click(object sender, RoutedEventArgs e)
        {
            if (Instagram.IsSessionFileExists())
            {
                File.Delete(Instagram.GetSessionFilePath());
            }
            await Instagram.Api.LogoutAsync();

            var authWnd = new AuthWnd();
            authWnd.Show();
            this.Close();
        }
        private void MenuSessionSaveAs_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDlg = new SaveFileDialog
            {
                Filter = "Binary files|*.bin",
                InitialDirectory = Directory.GetCurrentDirectory(),
                Title = "Saving of session file",
                AddExtension = true,
                DefaultExt = "bin"
            };

            if (saveFileDlg.ShowDialog() == true)
            {
                File.Copy(Instagram.GetSessionFilePath(), saveFileDlg.FileName, true);
            }
        }
        #endregion

        #region Parsing
        private async void BtnParsePendingRequests_Click(object sender, RoutedEventArgs e)
        {
            this.btnParsePendingRequests.IsEnabled = false;
            this.progressPendingRequestStatus.Value = this.progressPendingRequestStatus.Minimum;
            this.progressPendingRequestStatus.IsIndeterminate = true;

            try
            {
                var requests = await _toolKit.GetAllPendingDirectRequestThreads();
                this.lbPendingRequestAmount.Text = requests.Count.ToString();

                bool hasToSaveUsernames = this.checkPendingRequestsHasToSaveUsernames.IsChecked.Value;
                bool hasToSaveCsv = this.checkPendingRequestsHasToSaveCsv.IsChecked.Value;
                _toolKit.SaveUsersFromThreads(requests, "Pending Requests", hasToSaveUsernames, hasToSaveCsv);
            }
            catch (EmptyDirectRequests exception)
            {
                MessageBox.Show(exception.Message, "Empty requests", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            this.btnParsePendingRequests.IsEnabled = true;
            this.progressPendingRequestStatus.Value = this.progressPendingRequestStatus.Maximum;
            this.progressPendingRequestStatus.IsIndeterminate = false;
        }
        private async void BtnParseDirectChats_Click(object sender, RoutedEventArgs e)
        {
            this.btnParseDirectChats.IsEnabled = false;
            this.progressDirectChatStatus.Value = this.progressDirectChatStatus.Minimum;
            this.progressDirectChatStatus.IsIndeterminate = true;

            try
            {
                var chats = await _toolKit.GetAllDirectChatThreads();
                this.lbDirectChatUsersAmount.Text = chats.Count.ToString();

                bool hasToSaveUsernames = this.checkDirectChatHasToSaveDirectUsernames.IsChecked.Value;
                bool hasToSaveCsv = this.checkDirectChatHasToSaveCsv.IsChecked.Value;
                _toolKit.SaveUsersFromThreads(chats, "Direct chats", hasToSaveUsernames, hasToSaveCsv);
            }
            catch(EmptyDirectChats exception)
            {
                MessageBox.Show(exception.Message, "Empty chat list", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            this.btnParseDirectChats.IsEnabled = true;
            this.progressDirectChatStatus.Value = this.progressDirectChatStatus.Maximum;
            this.progressDirectChatStatus.IsIndeterminate = false;
        }
        #endregion

        #region Spammer
        private void BtnSpammerSelectInputFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDlg = new OpenFileDialog
            {
                Filter = "Save Files|*.bin",
                Multiselect = false
            };

            if (openFileDlg.ShowDialog() == true)
            {
                this.txtSpammerInputFilePath.Text = openFileDlg.FileName;

                var loadedUsers = _toolKit.Spammer.LoadSpammerUsers(this.txtSpammerInputFilePath.Text);

                this.lbSpammerUsersLeftCounter.Text = $"Users left: {loadedUsers.Count}";
                this.btnSpammerSendOnce.IsEnabled = true;
                this.btnSpammerSendToEndOfFile.IsEnabled = true;
            }
        }
        private async void BtnSpammerSendOnce_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.txtSpammerMessage.Text))
            {
                MessageBox.Show("Enter message text first!", "Empty message", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            UpdateUi(new Action(() => _toolKit.Spammer.Message = this.txtSpammerMessage.Text));

            var usersAmount = Convert.ToInt32(this.numSpammerUsersPerStack.Text);
            if (usersAmount > 300 || usersAmount < 1)
            {
                MessageBox.Show("The limit of sending is between 1 and 300 users!", "Users limit", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            this.btnSpammerSelectInputFile.IsEnabled = false;
            this.btnSpammerSendOnce.IsEnabled = false;
            this.btnSpammerSendToEndOfFile.IsEnabled = false;

            this.btnSpammerStopSending.IsEnabled = false;

            await _toolKit.Spammer.SendMessageToSeveralUsersAsync(usersAmount);

            this.btnSpammerSelectInputFile.IsEnabled = true;
            this.btnSpammerSendOnce.IsEnabled = true;
            this.btnSpammerSendToEndOfFile.IsEnabled = true;

            PrintSpammerLog($"Completed!");
            MessageBox.Show("All selected users got message!");
        }
        private async void BtnSpammerSendToEndOfFile_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.txtSpammerMessage.Text))
            {
                MessageBox.Show("Enter message text first!", "Empty message", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            UpdateUi(new Action(() => _toolKit.Spammer.Message = this.txtSpammerMessage.Text));
            UpdateUi(new Action(() => _toolKit.Spammer.PauseLength = Convert.ToInt32(this.numSpammerPauseLength.Text)));

            var usersAmount = Convert.ToInt32(this.numSpammerUsersPerStack.Text);
            if (usersAmount > 300 || usersAmount < 1)
            {
                MessageBox.Show("The limit of sending is between 1 and 300 users!", "Users limit", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            this.btnSpammerSelectInputFile.IsEnabled = false;
            this.btnSpammerSendOnce.IsEnabled = false;
            this.btnSpammerSendToEndOfFile.IsEnabled = false;
            this.btnSpammerStopSending.IsEnabled = true;

            await _toolKit.Spammer.SendMessageToAllUsersAsync(usersAmount);

            this.btnSpammerSelectInputFile.IsEnabled = true;
            this.btnSpammerSendOnce.IsEnabled = true;
            this.btnSpammerSendToEndOfFile.IsEnabled = true;
            this.btnSpammerStopSending.IsEnabled = false;

            PrintSpammerLog($"Completed!");
            MessageBox.Show("All users from the file got message!");
        }
        private void BtnSpammerStopSending_Click(object sender, RoutedEventArgs e)
        {
            _toolKit.Spammer.StopSending();
        }
        #endregion

        #region Converter
        private void BtnConverterUsernamesSelectFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDlg = new OpenFileDialog
            {
                Filter = "Text files|*.txt",
                InitialDirectory = Directory.GetCurrentDirectory(),
                Multiselect = false
            };

            if (openFileDlg.ShowDialog() == true)
            {
                this.txtConverterUsernamesFilePath.Text = openFileDlg.FileName;
                this.btnConverterUsernamesConvert.IsEnabled = true;
            }
            else
            {
                this.txtConverterUsernamesFilePath.Text = string.Empty;
                this.btnConverterUsernamesConvert.IsEnabled = false;
            }
        }
        private async void BtnConverterUsernamesConvert_Click(object sender, RoutedEventArgs e)
        {
            this.btnConverterUsernamesConvert.IsEnabled = false;
            this.btnConverterUsernamesSelectFile.IsEnabled = false;
            this.progressConverterUsernames.Value = this.progressConverterUsernames.Minimum;

            var convertedUsers = await _toolKit.ConvertUsernamesToShortUserInfo(this.txtConverterUsernamesFilePath.Text);

            var now = DateTime.Now;
            string prefix = $"{now.ToLongTimeString()}-{now.ToShortDateString()}"
                .Replace(':', '-');

            string dirName = "Converted Usernames";
            Directory.CreateDirectory(dirName);

            PrintConverterUsernamesLog($"Saving to {dirName}\\binary-users-{prefix}.bin.");
            _toolKit.WriteUsersToBinaryFile($"{dirName}\\binary-users-{prefix}.bin", convertedUsers);
            PrintConverterUsernamesLog($"Saving complete!");

            this.btnConverterUsernamesConvert.IsEnabled = true;
            this.btnConverterUsernamesSelectFile.IsEnabled = true;
        }
        #endregion
    }
}

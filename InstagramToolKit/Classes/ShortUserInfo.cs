﻿using ProtoBuf;

namespace InstagramToolKit.Classes
{
    [ProtoContract]
    public class ShortUserInfo
    {
        [ProtoMember(1)]
        public long Id { get; set; }
        [ProtoMember(2)]
        public string FullName { get; set; }
        [ProtoMember(3)]
        public string Username { get; set; }
    }
}

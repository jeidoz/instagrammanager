﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InstagramToolKit.Exceptions
{
    public class EmptyDirectChats : Exception
    {
        public EmptyDirectChats(string message)
            : base(message) { }

        public EmptyDirectChats(string message, Exception inner)
            : base(message, inner) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InstagramToolKit.Exceptions
{
    public class EmptyDirectRequests : Exception
    {
        public EmptyDirectRequests(string message)
            : base(message) {}

        public EmptyDirectRequests(string message, Exception inner)
            : base(message, inner) {}
    }
}

﻿using System;

namespace InstagramToolKit.Events
{
    public class AsyncProcessResult<T> : EventArgs
    {
        public T Message { get; set; }

        public AsyncProcessResult(T message)
        {
            Message = message;
        }
    }
}

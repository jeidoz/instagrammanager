﻿using InstagramApiSharp;
using InstagramApiSharp.Classes.Models;
using InstagramToolKit.Classes;
using InstagramToolKit.Events;
using InstagramToolKit.Exceptions;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramToolKit
{
    public class ToolKit
    {
        private readonly PaginationParameters _maxPages = PaginationParameters.MaxPagesToLoad(int.MaxValue);
        private readonly Semaphore _threadSemaphore = new Semaphore(1, 1);

        private List<string> _converterUsernames = new List<string>();

        public Spammer Spammer { get; } = new Spammer();

        public event EventHandler<AsyncProcessResult<string>> ConvertingLogEvent;
        public event EventHandler<AsyncProcessResult<int>> ConvertingProgressBarInitEvent;
        public event EventHandler<EventArgs> ConvertingUserProcessedEvent;
        

        public async Task<List<InstaDirectInboxThread>> GetAllPendingDirectRequestThreads()
        {
            var pendingDirectInbox = await Instagram.Api.MessagingProcessor.GetPendingDirectAsync(_maxPages);

            var threads = pendingDirectInbox.Value.Inbox.Threads;
            if (threads == null)
            {
                throw new EmptyDirectRequests("Your Direct messaging requests are empty.");
            }

            return threads;
        }
        public async Task<List<InstaDirectInboxThread>> GetAllDirectChatThreads()
        {
            var directInbox = await Instagram.Api.MessagingProcessor.GetDirectInboxAsync(_maxPages);

            var threads = directInbox.Value.Inbox.Threads;
            if (threads == null)
            {
                throw new EmptyDirectChats("Your list of Direct chats is empty.");
            }

            return threads;
        }

        public async Task<List<ShortUserInfo>> ConvertUsernamesToShortUserInfo(string fileWithUsernames, int stackSize = 220)
        {
            ConvertingLogEvent?.Invoke(this, new AsyncProcessResult<string>("Conveting has been started..."));

            _converterUsernames = File.ReadAllLines(fileWithUsernames).ToList();
            ConvertingProgressBarInitEvent?.Invoke(this, new AsyncProcessResult<int>(_converterUsernames.Count));
            var convertedUsers = new List<ShortUserInfo>();

            while (_converterUsernames.Count() > 0)
            {
                int amount = _converterUsernames.Count() > stackSize ? stackSize : _converterUsernames.Count();
                var preparedUsernames = _converterUsernames.Take(amount);

                await Task.WhenAll(preparedUsernames.Select(async username =>
                {
                    var userInfo = await Instagram.Api.UserProcessor.GetUserAsync(username);
                    
                    if (userInfo.Value != null)
                    {
                        _threadSemaphore.WaitOne();
                        _converterUsernames.Remove(username);
                        convertedUsers.Add(new ShortUserInfo
                        {
                            Id = userInfo.Value.Pk,
                            Username = userInfo.Value.UserName,
                            FullName = userInfo.Value.FullName
                        });
                        _threadSemaphore.Release();

                        ConvertingLogEvent?.Invoke(this, new AsyncProcessResult<string>($"Got @{username}"));
                    }
                    else
                    {
                        _threadSemaphore.WaitOne();
                        _converterUsernames.Remove(username);
                        _threadSemaphore.Release();
                        ConvertingLogEvent?.Invoke(this, new AsyncProcessResult<string>($"Problem with @{username}. User may not exist or try to wait 5mins and restart."));
                    }

                    ConvertingUserProcessedEvent?.Invoke(this, new EventArgs());
                }));

                if (_converterUsernames.Count() != 0)
                {
                    ConvertingLogEvent?.Invoke(this, new AsyncProcessResult<string>($"Waiting 10 seconds to evade http error 429..."));
                    await Task.Run(() => Thread.Sleep(TimeSpan.FromSeconds(10)));
                }
            }

            return convertedUsers;
        }

        public void SaveUsersFromThreads(List<InstaDirectInboxThread> threads, string resultsFolder = "Results", bool hasToSaveUsernames = false, bool hasToSaveCSV = false)
        {
            var pendingUsers = GetUsersFromRequests(threads);
            DateTime now = DateTime.Now;
            string prefix = $"{now.ToLongTimeString()}-{now.ToShortDateString()}"
                .Replace(':', '-');

            //It doesn't do anything if directory exist
            Directory.CreateDirectory(resultsFolder);

            WriteUsersToBinaryFile($"{resultsFolder}\\binary-users-{prefix}.bin", pendingUsers);

            if (hasToSaveUsernames)
            {
                WriteUsernamesToTxtFile($"{resultsFolder}\\usernames-{prefix}.txt", pendingUsers);
            }

            if (hasToSaveCSV)
            {
                WriteUsersToCsvFile($"{resultsFolder}\\users-{prefix}.csv", pendingUsers);
            }
        }
        private List<ShortUserInfo> GetUsersFromRequests(List<InstaDirectInboxThread> threads)
        {
            List<ShortUserInfo> pendingUsers = new List<ShortUserInfo>();
            foreach (var thread in threads)
            {
                foreach(var user in thread.Users)
                {
                    ShortUserInfo newUser = new ShortUserInfo
                    {
                        Id = user.Pk,
                        FullName = user.FullName,
                        Username = user.UserName
                    };
                    pendingUsers.Add(newUser);
                }
            }
            return pendingUsers;
        }

        public void WriteUsersToBinaryFile(string filePath, List<ShortUserInfo> users)
        {
            using (var file = File.Create(filePath))
            {
                Serializer.Serialize(file, users);
            }
        }
        private void WriteUsernamesToTxtFile(string filePath, List<ShortUserInfo> users)
        {
            var usernameRecords = users.Select(usr => usr.Username);
            File.AppendAllLines(filePath, usernameRecords);
        }
        private void WriteUsersToCsvFile(string filePath, IEnumerable<ShortUserInfo> users)
        {
            string header = "sep=;\nID;Username;\"Full name\"\n";
            var values = users.Select(user =>
            {
                string fullName = user.FullName.Replace("\"", "\"\"");
                return $"{user.Id};{user.Username};\"{fullName}\"\n";
            });

            string fileContent = header + string.Join(";", values);
            File.WriteAllText(filePath, fileContent);
        }    
    }
}

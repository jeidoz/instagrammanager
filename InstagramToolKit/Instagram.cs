﻿using InstagramApiSharp.API;
using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Classes;
using InstagramApiSharp.Classes.SessionHandlers;
using InstagramApiSharp.Logger;
using System;
using System.IO;

namespace InstagramToolKit
{
    public static class Instagram
    {
        public const string SessionFilename = "session.bin";
        public static IInstaApi Api;

        public static void InitializeApi()
        {
            Api = InstaApiBuilder.CreateBuilder()
                .UseLogger(new DebugLogger(LogLevel.All))
                .SetRequestDelay(RequestDelay.FromSeconds(0, 1))
                .SetSessionHandler(new FileSessionHandler() { FilePath = SessionFilename })
                .Build();
        }
        public static void InitializeApi(UserSessionData userSessionData)
        {
            Api = InstaApiBuilder.CreateBuilder()
                .SetUser(userSessionData)
                .UseLogger(new DebugLogger(LogLevel.All))
                .SetRequestDelay(RequestDelay.FromSeconds(0, 1))
                .SetSessionHandler(new FileSessionHandler() { FilePath = SessionFilename })
                .Build();
        }

        public static void LoadSession()
        {
            Api?.SessionHandler?.Load();
        }
        public static void SaveSession()
        {
            if (Api == null)
                return;
            if (!Api.IsUserAuthenticated)
                return;
            Api.SessionHandler.Save();
        }
        public static string GetSessionFilePath()
        {
            var currentDir = Directory.GetCurrentDirectory();
            return Path.Combine(currentDir, SessionFilename);
        }
        public static bool IsSessionFileExists()
        {
            return File.Exists(GetSessionFilePath());
        }

        public static void InitializeLogging(object sender)
        {
            FileStream outputStream;
            StreamWriter writer;
            try
            {
                var now = DateTime.Now;
                string prefix = $"{now.ToLongTimeString()}-{now.ToShortDateString()}"
                    .Replace(':', '-');

                Directory.CreateDirectory("Logs");

                outputStream = new FileStream($"Logs\\{sender.GetType().Name}-{prefix}.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(outputStream);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open Redirect.txt for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);
            //writer.Close();
            //outputStream.Close();
        }
    }
}

﻿using InstagramToolKit.Classes;
using InstagramToolKit.Events;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InstagramToolKit
{
    public class Spammer
    {
        private readonly Semaphore _threadSemaphore = new Semaphore(1, 1);

        private string _currentUsersSaveFile;
        private List<ShortUserInfo> _spammerUsers = new List<ShortUserInfo>();
        private bool _isSendingStoped = false;

        public string Message { get; set; }
        public int PauseLength { get; set; }

        public event EventHandler<AsyncProcessResult<string>> UserGotMessageEvent;
        public event EventHandler<AsyncProcessResult<string>> SavingLeftUsersEvent;
        public event EventHandler<AsyncProcessResult<int>> ChangedLeftUsersAmountEvent;
        public event EventHandler<AsyncProcessResult<string>> SendingStopedEvent;
        public event EventHandler<AsyncProcessResult<string>> PauseStartedEvent;

        public List<ShortUserInfo> LoadSpammerUsers(string filePath)
        {
            using (var file = File.OpenRead(filePath))
            {
                _spammerUsers = Serializer.Deserialize<List<ShortUserInfo>>(file);
            }
            RemoveDublicates(ref _spammerUsers);

            _currentUsersSaveFile = filePath;
            ChangedLeftUsersAmountEvent?.Invoke(this, new AsyncProcessResult<int>(_spammerUsers.Count));
            return _spammerUsers;
        }
        private void RemoveDublicates(ref List<ShortUserInfo> originList)
        {
            var dublicatelessList = new List<ShortUserInfo>();

            while (originList.Count > 0)
            {
                var lastOriginItem = originList.Last();
                dublicatelessList.Add(lastOriginItem);
                originList.RemoveAll(item => item.Id == lastOriginItem.Id);
            }

            originList = dublicatelessList;
        }

        public void SaveLeftSpammerUsers()
        {
            if (File.Exists(_currentUsersSaveFile))
                File.Delete(_currentUsersSaveFile);

            if (_spammerUsers.Count() > 0)
            {
                using (var file = File.Open(_currentUsersSaveFile, FileMode.CreateNew))
                {
                    Serializer.Serialize(file, _spammerUsers);
                }
            }
        }

        public void StopSending()
        {
            _isSendingStoped = true;
        }

        public async Task SendMessageToAllUsersAsync(int usersAmountPerStack)
        {
            _isSendingStoped = false;

            while (_spammerUsers.Count > 0)
            {
                if (_isSendingStoped)
                {
                    SaveLeftSpammerUsers();
                    SendingStopedEvent?.Invoke(this, new AsyncProcessResult<string>("Message sending has been stopped."));
                    return;
                }

                await SendMessageToSeveralUsersAsync(usersAmountPerStack);

                if (_spammerUsers.Count > 0)
                {
                    PauseStartedEvent?.Invoke(this, new AsyncProcessResult<string>($"Waiting {this.PauseLength} seconds to evade http error 429..."));
                    await Task.Delay(TimeSpan.FromSeconds(this.PauseLength));
                }
            }
        }
        public async Task SendMessageToSeveralUsersAsync(int usersAmount)
        {
            _isSendingStoped = false;

            var preparedUsers = PrepareSpammerUsersForSending(usersAmount);
            List<Task> tasks = new List<Task>();

            foreach (var user in preparedUsers)
            {
                tasks.Add(SendMessage(user));
            }

            await Task.WhenAll(tasks);
            SavingLeftUsersEvent?.Invoke(this, new AsyncProcessResult<string>($"Saving... ({_spammerUsers.Count} users left)"));
            SaveLeftSpammerUsers();
        }
        private List<ShortUserInfo> PrepareSpammerUsersForSending(int usersAmountPerStack)
        {
            int amount = (_spammerUsers.Count > usersAmountPerStack) ? usersAmountPerStack : _spammerUsers.Count;
            return new List<ShortUserInfo>(_spammerUsers.Take(amount));
        }
        public async Task SendMessage(ShortUserInfo user)
        {
            await Instagram.Api.MessagingProcessor.SendDirectTextAsync(user.Id.ToString(), null, Message);

            _threadSemaphore.WaitOne();
            _spammerUsers.Remove(user);
            _threadSemaphore.Release();

            UserGotMessageEvent?.Invoke(this, new AsyncProcessResult<string>($"User @{user.Username} got message"));
            ChangedLeftUsersAmountEvent?.Invoke(this, new AsyncProcessResult<int>(_spammerUsers.Count));
        }
    }
}
